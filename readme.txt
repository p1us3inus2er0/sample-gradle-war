【前提】
プロキシサーバの設定が必要な環境の場合は、
「gradle.properties.org」を「gradle.properties」にリネームして、
プロキシサーバのIP、PORTを設定する。

【コマンド】(コマンドプロンプト or ターミナル)
# クリーン
gradlew clean

# ビルド(eclipseを使用している場合は自動ビルドなので不要。)
gradlew classes

# tomcat開始
gradlew tomcatRun

# tomcat開始(デバッグ)
set GRADLE_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n
gradlew tomcatRun

# tomcat終了(tomcatRunsしたプロンプトをCtrl+CでもOK。)
gradlew tomcatStop

# war作成
gradlew war

# tomcat開始(war)
gradlew tomcatRunWar

【eclipseを使う場合】
・初回のみ
ウィンドウ→設定→java→ビルド・パス→クラスパス変数[GRADLE_USER_HOME]を追加。
e.g.
 windows　C:/Documents and Settings/【ユーザ名】/.gradle
 mac　/Users/【ユーザ名】/.gradle

# jarを追加した場合
gradlew cleanEclipse eclipse

# リモートデバッグ
実行→デバッグの構成→リモートjavaアプリケーション
該当プロジェクトを指定して、デバッグ実行

【備忘】
tomcatのバージョン7.0.27〜7.0.39では、reload時にNullPointerExceptionが発生する。
そのため、バージョンを7.0.26に。
